<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {

    Route::get('/users', 'UserController@index');
    Route::post('/users-store', 'UserController@store');
    Route::post('/users-update/{id}', 'UserController@update');
    Route::get('/users-destroy/{id}', 'UserController@destroy');
});

// Se um endpoint nao existir
Route::fallback(function(){
    return response()->json([
        'message' => 'Page Not Found. If error persists, contact the webmaster.'], 404);
});
