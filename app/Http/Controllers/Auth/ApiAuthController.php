<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ApiAuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:2',
        ]);

        if ($validator->fails())
        {
            return response(['message' => $validator->errors()->all()], 200);
        }

        $user = User::where('email', $request->email)->first();
        if ($user) {
            $credentials = request(['email', 'password']);
            if (Auth::attempt($credentials)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $response = [
                    'token' => $token,
                    'user' => $user
                ];
                return response($response, 200);
            } else {
                $response = ["message" => ["Password errada"]];
                return response($response, 200);
            }
        } else {
            $response = ["message" => ['User não existe']];
            return response($response, 200);
        }
    }

    public function logout (Request $request) {
        if(Auth::user()) {
            $token = $request->user()->token();
            $token->revoke();
            $response = ['message' => ['O logout foi feito com sucesso!']];
            return response($response, 200);
        }else{
            $response = ['message' => ['não foi possível fazer Logout!']];
            return response($response, 200);
        }
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:2|confirmed',
        ]);

        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }

        $request['password']=Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);
        $user = User::create($request->toArray());
        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $response = ['token' => $token];

        return response($response, 200);
    }
}
