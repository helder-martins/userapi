<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $users = User::all();

            return response()->json([
                'users' => $users
            ],  200);
        } catch (\Exception $e) {

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:2',
            ]);

            if ($validator->fails()) {
                return response(['errors' => $validator->errors()->all()], 400);

            } else {
                $request['password'] = Hash::make($request['password']);
                $user = User::create($request->all());
                $user->createToken('Laravel Password Grant Client')->accessToken;
                $response = ['message' => ['Utilizador criado com sucesso!']];

                $user->save();

                return response($response, 200);
            }

        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 406);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = User::find($id);

            if (is_null($user)) {
                return response()->json([
                    'message' => 'Utilizador não encontrado'
                ], 404);

            } else {
                $user->update($request->all());

                $response = ['message' => ['Utilizador actualizado com sucesso!']];

                return response()->json(
                    $response,
                    200);
            }

        } catch (\Exception $e) {

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = User::find($id);

            if (is_null($user)) {
                return response()->json([
                    'message' => 'Utilizador não encontrado'
                ], 404);

            } else {
                $user->delete();
                $response = ['message' => ['Utilizador removido com sucesso!']];

                return response()->json(
                    $response,
                    200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
                ], 406);
        }
    }
}
