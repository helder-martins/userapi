## Setup do projecto

Instalar node modules:

```
npm install
```

Instalar laravel dependencies:

```
composer install
```

Correr artisan server. Vai correr em localhost:8000:

```
php artisan serve
```

Setup da base de dados no ficheiro .env:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=usersdb
DB_USERNAME=your_username
DB_PASSWORD=your_password
```

Correr os seguintes comandos para fazer a migration e o seed da base de dados:

```
php artisan migrate
```
```
php artisan db:seed --class=DatabaseSeeder
```

Instalar o Passport

```
composer require laravel/passport
```
```
php artisan passport:install
```

Colocar o Client1 e o Client2 no ficheiro .env:

```
CLIENT_1=YOUR_CLIENT_1
CLIENT_2=YOUR_CLIENT_2
```

Criar um storage link entre public folder e storage folder

```
php storage:link
```

Neste ponto, o projecto já estará a correr em http:localhost:8000.
